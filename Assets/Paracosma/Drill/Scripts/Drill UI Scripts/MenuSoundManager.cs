﻿using UnityEngine;
using UnityEngine.UI;

public class MenuSoundManager : MonoBehaviour
{
    
    public GameObject MusicButton;
    public Sprite musicOn, musicOff;
    public AudioSource audiosource, bgSource;
    public AudioClip menuBgClip, drillBgClip;
    public AudioClip clickClip,startBtnClip;
    bool MusicOnOff = false;
    //toggle menu bg music on and off
    public void MusicToggle()
    {
        MusicOnOff = !MusicOnOff;

        if (MusicOnOff)
        {
            MusicButton.GetComponent<Image>().sprite = musicOff;
            bgSource.Pause();
        }
        else
        {
            MusicButton.GetComponent<Image>().sprite = musicOn;
            bgSource.Play();
        }
    }
    //button sounds
    public void Click() => audiosource.PlayOneShot(clickClip);
    public void StartBtnClick() => audiosource.PlayOneShot(startBtnClip);
    

}
