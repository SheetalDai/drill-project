﻿using UnityEngine;

public class LineConnecter : MonoBehaviour
{
    LineRenderer lr;
    public Transform[] pos;
    public GameObject[] bones;
    public bool test;

    void Start()
    {
        lr = GetComponent<LineRenderer>();
    }

    void Update()
    {
        lr.positionCount = pos.Length;
        for (int i = 0; i < pos.Length; i++)        
            if (pos[i] != null)
                lr.SetPosition(i, pos[i].position);        
    }
}
