﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;
using System.Collections;

public class MenuController : MonoBehaviour
{
    public int Itemvalue;
    public GameObject Content;
    public GameObject rightPanelImgHolder;
    public GameObject downPanelStartBtn;
    public Text rightPanelTopicText, rightPanelDescription;
    public Text downPanelText;//down panel    
    public Text timeText, greetingText;//timer

    public delegate void EnableBorder(int num);
    public static event EnableBorder onMenuItemClicked;

    private static MenuController _instance;
    public static MenuController Instance { get { return _instance; } }
    private void Start()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
        
    }

    void Update()
    {
        Time();
    }

    public void SelectItem(int value)
    {
        Itemvalue = value;
        downPanelText.gameObject.SetActive(false);
        downPanelStartBtn.SetActive(true);


        //change rightpanel image accordance to clicked item
        var _image = rightPanelImgHolder.transform.GetChild(0).GetComponent<Image>();
        var _contentChild0 = Content.transform.GetChild(Itemvalue);
        _image.sprite = _contentChild0.transform.GetChild(0).transform.GetComponent<Image>().sprite;
        _image.gameObject.SetActive(true);


        //change rightpanel topic text and description 
        rightPanelTopicText.text = HandToolsData.instance.handToolsName[Itemvalue];//change topic text
        rightPanelDescription.text = HandToolsData.instance.handToolsDescription[Itemvalue];//change description text

        onMenuItemClicked(value);
    }
    //loading new selected scene
    public void StartBtn() => StartCoroutine(LoadScene(Itemvalue+1));
    //exiting application
    public void ExitBtn() => Application.Quit();

    IEnumerator LoadScene(int scene)
    {
        yield return null;
        //Begin to load the Scene you specify
        AsyncOperation asyncOperation = SceneManager.LoadSceneAsync(scene);
        //Don't let the Scene activate until you allow it to
        asyncOperation.allowSceneActivation = false;
        while (!asyncOperation.isDone)
        {
            // Check if the load has finished
            if (asyncOperation.progress >= 0.9f)
            {
                //Activate the Scene
                asyncOperation.allowSceneActivation = true;
            }

            yield return null;
        }
    }
    //time for devices
    public void Time()
    {
        var _dateTime = DateTime.Now.ToString("h:mm");

        if (DateTime.Now.Hour >= 12 && DateTime.Now.Hour < 24)
        {
            timeText.text = _dateTime + " PM ";
            greetingText.text = "Good Afternoon,";
        }
        else if(DateTime.Now.Hour < 12 || DateTime.Now.Hour ==24)
        {
            timeText.text = _dateTime + " AM ";
            greetingText.text = "Good Morning,";
        }
    }
}
