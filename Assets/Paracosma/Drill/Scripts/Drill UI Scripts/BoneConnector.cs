﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoneConnector : MonoBehaviour
{
    public GameObject[] bones;
    // Start is called before the first frame update
    void Start()
    {
        for(int i = 1; i<bones.Length; i++)
        {
            bones[i].GetComponent<CharacterJoint>().connectedBody = gameObject.transform.GetChild(i-1).GetComponent<Rigidbody>();
        }
    }

    // Update is called once per frame
    void Update()
    {

        
        
    }
}
