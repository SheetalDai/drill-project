﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HandToolsData : MonoBehaviour
{
    public static HandToolsData instance;
    public int TotalItemNumbers;
    public int value;
    public string[] handToolsName;
    public string[] handToolsDescription;

    public Sprite uiImage;
    // Start is called before the first frame update
    void Start()
    {
        instance = this;
        TotalItemNumbers=MenuController.Instance.Content.transform.childCount;
        //loading handtools data to menu ui
        for (int i = 0; i < TotalItemNumbers; i++)
        {
            value = i;
            Data();
            //set ui image
            MenuController.Instance.Content.transform.GetChild(i).transform.GetChild(0).GetComponent<Image>().sprite = uiImage;
            //set ui text hand tools
            MenuController.Instance.Content.transform.GetChild(i).transform.GetChild(2).transform.GetChild(0).GetComponent<Text>().text = handToolsName[i];
        }
        
    }
    //hand tools data
    public void Data()
    {
        switch (value)
        {
            case 0:
                
                handToolsName[0] = "Drill";
                uiImage = Resources.Load<Sprite>(handToolsName[0]);//img name should be same as handToolsName
                handToolsDescription[0] = "In this simulation, trainees learn and demonstrates proper procedures for use of a drill. After learning the proper procedures for use of a drill, the trainee demonstrates their knowledge with corrective feedback from the virtual coach to guide the trainee toward mastery.";
                break;
            case 1:
                handToolsName[1] = "Circular Saw";
                uiImage = Resources.Load<Sprite>(handToolsName[1]);
                handToolsDescription[1] = "In this simulation, trainees learn and demonstrates proper procedures for use of a circular saw. After learning the proper procedures for use of a circular saw, the trainee demonstrates their knowledge with corrective feedback from the virtual coach to guide the trainee toward mastery.";
                break;
            case 2:
                handToolsName[2] = "Angle Grinder";
                uiImage = Resources.Load<Sprite>(handToolsName[2]);
                handToolsDescription[2] = "In this simulation, trainees learn and demonstrates proper procedures for use of an angle grinder. After learning the proper procedures for use of an angle grinder, the trainee demonstrates their knowledge with corrective feedback from the virtual coach to guide the trainee toward mastery.";
                break;
            case 3:
                handToolsName[3] = "Jigsaw";
                uiImage = Resources.Load<Sprite>(handToolsName[3]);
                handToolsDescription[3] = "In this simulation, trainees learn and demonstrates proper procedures for use of a jigsaw. After learning the proper procedures for use of a jigsaw, the trainee demonstrates their knowledge with corrective feedback from the virtual coach to guide the trainee toward mastery.";
                break;
            case 4:
                handToolsName[4] = "Chainsaw";
                uiImage = Resources.Load<Sprite>(handToolsName[4]);
                handToolsDescription[4] = "In this simulation, trainees learn and demonstrates proper procedures for use of a chainsaw. After learning the proper procedures for use of a chainsaw, the trainee demonstrates their knowledge with corrective feedback from the virtual coach to guide the trainee toward mastery.";
                break;
            case 5:
                handToolsName[5] = "Impact Wrench";
                uiImage = Resources.Load<Sprite>(handToolsName[5]);
                handToolsDescription[5] = "In this simulation, trainees learn and demonstrates proper procedures for use of an impact wrench. After learning the proper procedures for use of an impact wrench, the trainee demonstrates their knowledge with corrective feedback from the virtual coach to guide the trainee toward mastery.";
                break;
            case 6:
                handToolsName[6] = "Hammer";
                uiImage = Resources.Load<Sprite>(handToolsName[6]);
                handToolsDescription[6] = "In this simulation, trainees learn and demonstrates proper procedures for use of a hammer. After learning the proper procedures for use of a hammer, the trainee demonstrates their knowledge with corrective feedback from the virtual coach to guide the trainee toward mastery.";
                break;
            case 7:
                handToolsName[7] = "Screwdriver";
                uiImage = Resources.Load<Sprite>(handToolsName[7]);
                handToolsDescription[7] = "In this simulation, trainees learn and demonstrates proper procedures for use of a screwdriver. After learning the proper procedures for use of a screwdriver, the trainee demonstrates their knowledge with corrective feedback from the virtual coach to guide the trainee toward mastery.";
                break;
            case 8:
                handToolsName[8] = "Utility Knife";
                uiImage = Resources.Load<Sprite>(handToolsName[8]);
                handToolsDescription[8] = "In this simulation, trainees learn and demonstrates proper procedures for use of a utility knife. After learning the proper procedures for use of a utility knife, the trainee demonstrates their knowledge with corrective feedback from the virtual coach to guide the trainee toward mastery.";
                break;
            case 9:
                handToolsName[9] = "Chisel";
                uiImage = Resources.Load<Sprite>(handToolsName[9]);
                handToolsDescription[9] = "In this simulation, trainees learn and demonstrates proper procedures for use of a chisel. After learning the proper procedures for use of a chisel, the trainee demonstrates their knowledge with corrective feedback from the virtual coach to guide the trainee toward mastery.";
                break;

        }
    }
}
