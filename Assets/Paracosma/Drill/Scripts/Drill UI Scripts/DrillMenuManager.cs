﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using VRTK;

public class DrillMenuManager : MonoBehaviour
{
    public GameObject MenuCanvas;
    public GameObject[] instructions;

    private static DrillMenuManager _instance;
    public VRTK_StraightPointerRenderer stPointer;
    public static DrillMenuManager Instance { get { return _instance; } }
    void Start()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
        Invoke(nameof(MenuOn), 1f);
    }
    private void Update()
    {
        //turning pointer on when task completes if drill is not grabbed
        if (Nail.instance.pointerOn)
        {
            if (Drill.Instance.linkedObject.IsGrabbed())
            {
                stPointer.tracerVisibility = VRTK_StraightPointerRenderer.VisibilityStates.OnWhenActive;
                stPointer.cursorVisibility = VRTK_StraightPointerRenderer.VisibilityStates.OnWhenActive;
            }
            else
            {
                stPointer.tracerVisibility = VRTK_StraightPointerRenderer.VisibilityStates.AlwaysOn;
                stPointer.cursorVisibility = VRTK_StraightPointerRenderer.VisibilityStates.AlwaysOn;
            }

        }

    }
    //drill menu turn on and off
    public void MenuOn()
    {
        MenuCanvas.SetActive(true);
        if (Nail.instance.isSuccess)
        {
            DrillSoundManager.Instance.SuccessClip();
            Nail.instance.isSuccess = false;        }
        else
        {
            DrillSoundManager.Instance.PopInClip();
        }    }
    public void MenuOff()
    {
        MenuCanvas.SetActive(false);
        DrillSoundManager.Instance.PopOutClip();
        Invoke(nameof(MenuOn), 1f);
    }
    //turning menu off and turning new menu info on accordingly
    public void MenuOffActivateInstruction(int num)
    {
        MenuOff();

        for (int i = 0; i < instructions.Length; i++)
        {
            if (i == num)
                instructions[i].SetActive(true);
            else
                instructions[i].SetActive(false);
        }
    }

    //exit current drill scene and opens menu scene
    public void ExitBtn()
    {
        StartCoroutine(LoadScene(0));
    }
    //reopen same scene
    public void ReplayBtn()
    {
        StartCoroutine(LoadScene(1));
    }
    IEnumerator LoadScene(int scene)
    {
        yield return null;
        //Begin to load the Scene you specify
        AsyncOperation asyncOperation = SceneManager.LoadSceneAsync(scene);
        //Don't let the Scene activate until you allow it to
        asyncOperation.allowSceneActivation = false;
        while (!asyncOperation.isDone)
        {
            // Check if the load has finished
            if (asyncOperation.progress >= 0.9f)
            {
                    //Activate the Scene
                    asyncOperation.allowSceneActivation = true;
            }

            yield return null;
        }
    }
}
