﻿using UnityEngine;
using UnityEngine.UI;

public class Boarder : MonoBehaviour
{
    // Start is called before the first frame update
    public int index;
    void Start()
    {
        transform.GetComponent<Image>().enabled = false;

        MenuController.onMenuItemClicked += MenuController_onMenuItemClicked;
    }
    //turning on and off for selected boarder on ui Menu
    private void MenuController_onMenuItemClicked(int num)
    {
        if (num == index) transform.GetComponent<Image>().enabled = true;
        else transform.GetComponent<Image>().enabled = false;
    }
    private void OnDisable()
    {
        MenuController.onMenuItemClicked -= MenuController_onMenuItemClicked;
    }
}
