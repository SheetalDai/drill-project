﻿using UnityEngine;

public class PreciseSnapper : MonoBehaviour
{
    public Vector3 customRot;
    public Vector3 PosAdjuster;
    public GameObject OriginalGO;
    public GameObject Target;

    private static PreciseSnapper _instance;
    public static PreciseSnapper Instance { get { return _instance; } }
    void Start()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
        Target = GameObject.Find("Philips_01");
    }
    void Update()
    {
        if (Target == null)
        {
            Target = GameObject.Find("Philips_01");
        }
        if (Drill.Instance != null)
        {
            if (Drill.Instance.isGrabbed)
            {
                if (DrillMachine.Instance.lastTouch)
                {
                    AlignDrillPos(true);
                }
                else
                {
                    AlignDrillPos(false);
                }
            }
            else
            {
                AlignDrillPos(false);
            }
        }
    }
    public void AlignDrillPos(bool ok)
    {
        if (ok)
        {
            gameObject.transform.position = Target.transform.position + PosAdjuster;
            gameObject.transform.eulerAngles = new Vector3(transform.eulerAngles.x, transform.eulerAngles.y, transform.eulerAngles.z);
        }
        else
        {
            gameObject.transform.position = OriginalGO.transform.position;
            gameObject.transform.eulerAngles = OriginalGO.transform.eulerAngles;
        }
    }
}
