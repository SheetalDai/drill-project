﻿using UnityEngine;
using VRTK;
public class DrillMachine : VRTK_InteractableObject
{
    private RaycastHit touch;
    public bool lastTouch;
    private static DrillMachine _instance;
    public static DrillMachine Instance { get { return _instance; } }
    private void Start()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }
    void Update()
    {
        float tipHeight = transform.Find("Tip").transform.localScale.x;
        Vector3 tip = transform.Find("Tip").transform.position;

        if (lastTouch)
            tipHeight += 0.1f;

        if (Physics.Raycast(tip, transform.right, out touch, tipHeight))
        {
            if (!touch.collider.CompareTag("Nail"))
                return;

            if (!lastTouch)
            {
                lastTouch = true;
            }
        }
        else
        {
            lastTouch = false;
        }
    }
}
