﻿using UnityEngine;

public class DrillSpawner : MonoBehaviour
{
    public GameObject spawnPoint;
    public float distance;

    private void Update()
    {
        if (!Drill.Instance.linkedObject.IsGrabbed())
        {
            if (transform.position.y <= distance)
            {
                Destroy(gameObject);
            }
        }

        if (!Drill.Instance.linkedObject.IsGrabbed() && Drill.Instance.isConnected)
        {
            if (transform.position.y <= distance)
            {
                Destroy(gameObject);
                Spawner.spawnWire = true;                
            }
        }

        if (Spawner.destroyDrill)
        {
            Spawner.destroyDrill = false;
            Destroy(gameObject);
        }

    }
    private void OnDisable()
    {
        Spawner.spawnDrill = true;
    }
}
