﻿using UnityEngine;

public class Spawner : MonoBehaviour
{
    public GameObject drillPrefab, wirePrefab;
    public GameObject drillSpawnPos, wireSpawnPos;
    public static bool spawnDrill, spawnWire;
    public static bool destroyDrill;

    private void Start()
    {
        InstantiateDrill();
        InstantiateWire();
    }

    // Update is called once per frame
    void Update()
    {
        if (spawnDrill)
        {
            spawnDrill = false;
            var _drill = GameObject.FindGameObjectsWithTag("Drill");
            foreach (var item in _drill)
            {
                if (item != null)
                    Destroy(item);
            }
            InstantiateDrill();
        }
        if (spawnWire)
        {
            spawnWire = false;
            var _wire = GameObject.FindGameObjectsWithTag("Cable");
            foreach (var item in _wire)
            {
                if (item != null)
                    Destroy(item);
            }
            InstantiateWire();
        }
    }

    void InstantiateDrill()
    {
        var drill = Instantiate(drillPrefab, drillSpawnPos.transform.localPosition, Quaternion.identity);
        drill.transform.localRotation = drillSpawnPos.transform.localRotation;
    }
    void InstantiateWire()
    {
        var wire = Instantiate(wirePrefab, wireSpawnPos.transform.localPosition, Quaternion.identity);
        wire.transform.localRotation = wireSpawnPos.transform.localRotation;
    }
}
