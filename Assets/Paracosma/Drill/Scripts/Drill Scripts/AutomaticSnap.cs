﻿using UnityEngine;
using VRTK;

public class AutomaticSnap : MonoBehaviour
{
    VRTK_SnapDropZone sZone;
    public static bool haptic;
    private void Start()
    {
        sZone = GetComponent<VRTK_SnapDropZone>();
    }
    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("SnapCylinder"))
        {
            if (sZone.ValidSnappableObjectIsHovering())
            {
                sZone.ForceSnap(other.gameObject);
                haptic = true;
                DrillSoundManager.Instance.HosePluggedClip();
            }
        }        
    }
    private void OnTriggerStay(Collider other)
    {
        if(other.CompareTag("SnapCylinder"))
        {
            if (sZone.ValidSnappableObjectIsHovering() && !Drill.Instance.linkedObject.IsGrabbed())
            {
                sZone.ForceSnap(other.gameObject);                
            }
        }
        
    }
    private void OnTriggerExit(Collider other)
    {
        if(other.CompareTag("SnapCylinder"))
        {
            Drill.Instance.isDrilliing = false;
            DrillSoundManager.Instance.StopDrill();            
        }        
    }

    private void OnDisable()
    {
        Drill.Instance.isDrilliing = false;
    }
}
