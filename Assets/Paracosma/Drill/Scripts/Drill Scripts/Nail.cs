﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Nail : MonoBehaviour
{
    public static Nail instance;
    public bool isNailed;
    public GameObject table;
    public float distance;
    public bool isReadyToBeNailed, isSuccess, pointerOn;

    private void Start()
    {
        instance = this;
    }
    void Update()
    {
        if (!isNailed)
        {
            if (Drill.Instance.isDrilliing)
            {
                if (DrillMachine.Instance.lastTouch)//(isReadyToBeNailed)
                {
                    NailIn();
                }
            }
        }
    }

    public void NailIn()
    {
        float dis = Vector3.Distance(transform.position, table.transform.position);

        if (dis <= distance)
        {
            isNailed = false;
            transform.position += new Vector3(0, -.01f * Time.deltaTime, 0);
            transform.localRotation = Quaternion.Euler(-90, (Time.deltaTime) * 10000, 0);
        }
        else
        {
            isNailed = true; //screw complete            
            isSuccess = true;
            pointerOn = true;
            DrillSoundManager.Instance.HosePluggedClip();
            DrillMenuManager.Instance.MenuOffActivateInstruction(2);
        }
    }
}
