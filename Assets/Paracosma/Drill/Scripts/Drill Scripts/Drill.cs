﻿using UnityEngine;
using VRTK;

public class Drill : MonoBehaviour
{
    public VRTK_InteractableObject linkedObject;
    public GameObject chuck, trigger;
    public int rotationSpeed;
    public bool isDrilliing, isGrabbed, isConnected;
    public GameObject snapDropJoint;
    protected VRTK_ControllerEvents rightEvents, leftEvents;
    public bool showedInfo;

    private static Drill _instance;
    public static Drill Instance { get { return _instance; } }

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }
    private void Start()
    {
        rightEvents = GameObject.Find("RightControllerScriptAlias").GetComponent<VRTK_ControllerEvents>();
        leftEvents = GameObject.Find("LeftControllerScriptAlias").GetComponent<VRTK_ControllerEvents>();

        rightEvents.TriggerPressed += ControllerEvents_TriggerPressed;
        rightEvents.TriggerReleased += ControllerEvents_TriggerReleased;

        leftEvents.TriggerPressed += LeftEvents_TriggerPressed;
        leftEvents.TriggerReleased += LeftEvents_TriggerReleased;
    }

    private void LeftEvents_TriggerReleased(object sender, ControllerInteractionEventArgs e) => TriggerReleased(leftEvents);
    private void LeftEvents_TriggerPressed(object sender, ControllerInteractionEventArgs e) => TriggerPressed(leftEvents);

    private void ControllerEvents_TriggerReleased(object sender, ControllerInteractionEventArgs e) => TriggerReleased(rightEvents);
    private void ControllerEvents_TriggerPressed(object sender, ControllerInteractionEventArgs e) => TriggerPressed(rightEvents);

    private void Update()
    {
        if (linkedObject.IsGrabbed())
        {
            isGrabbed = true;
            if (snapDropJoint.transform.GetChild(snapDropJoint.transform.childCount - 1).CompareTag("SnapCylinder"))
            {
                isConnected = true;
                if (!showedInfo && !Nail.instance.isNailed)
                {
                    showedInfo = true;
                    DrillMenuManager.Instance.MenuOffActivateInstruction(1);
                }
            }
            else
            {
                isConnected = false;
            }
        }
        else
        {
            DrillSoundManager.Instance.StopDrill();
            isGrabbed = false;
        }

        Drilling(rightEvents);
        Drilling(leftEvents);
    }
    private void Drilling(VRTK_ControllerEvents e)
    {
        if (linkedObject.IsGrabbed(e.gameObject))
        {
            if (isDrilliing)
            {
                float power = e.GetTriggerAxis();

                if (chuck != null)
                    chuck.transform.Rotate(new Vector3(1, 0, 0) * Mathf.Sin(Time.deltaTime) * rotationSpeed);

                //increasing power of haptics if screw and drill is in contact 
                float increasePower = 1;
                if (DrillMachine.Instance.lastTouch)
                    increasePower = 10000;

                VRTK_ControllerHaptics.TriggerHapticPulse(VRTK_ControllerReference.GetControllerReference(e.gameObject), power * 0.25f * increasePower, 0.1f, 0.01f);
            }
        }
    }
    private void TriggerReleased(VRTK_ControllerEvents hand)
    {
        if (linkedObject != null)
        {
            if (linkedObject.IsGrabbed(hand.gameObject))
            {
                if (trigger.transform.localPosition.x != 0.8424205f)//8424205
                    trigger.transform.localPosition = new Vector3(0.8424205f, trigger.transform.localPosition.y, trigger.transform.localPosition.z);

                if (snapDropJoint.transform.GetChild(snapDropJoint.transform.childCount - 1).CompareTag("SnapCylinder"))
                    DrillSoundManager.Instance.PlayDrillClip3();
            }
        }
        isDrilliing = false;
    }
    private void TriggerPressed(VRTK_ControllerEvents hand)
    {
        if (linkedObject != null)
        {
            if (linkedObject.IsGrabbed(hand.gameObject))
            {
                if (trigger.transform.localPosition.y != 0.771f)//8424205
                    trigger.transform.localPosition = new Vector3(0.771f, trigger.transform.localPosition.y, trigger.transform.localPosition.z);

                if (snapDropJoint.transform.GetChild(snapDropJoint.transform.childCount - 1).CompareTag("SnapCylinder"))
                {
                    isDrilliing = true;

                    if (Nail.instance.pointerOn)
                    {
                        if (DrillMachine.Instance.lastTouch)
                            DrillSoundManager.Instance.PlayDrillMetallic();
                        else
                            DrillSoundManager.Instance.PlayDrillClip2();
                    }
                    else
                    {
                        DrillSoundManager.Instance.PlayDrillClip2();
                    }
                }
                else
                {
                    isDrilliing = false;
                }
            }
        }
    }
}