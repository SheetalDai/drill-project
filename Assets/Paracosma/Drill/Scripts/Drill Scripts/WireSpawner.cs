﻿using UnityEngine;
using VRTK;
public class WireSpawner : MonoBehaviour
{
    public static WireSpawner instance;
    public VRTK_InteractableObject wire;
    public float distance;

    private void Start()
    {
        instance = this;
    }
    private void Update()
    {
        if (wire != null)
        {
            if (!wire.IsGrabbed() && !Drill.Instance.isConnected)
            {
                if (wire.transform.position.y <= distance)
                {
                    Spawner.spawnWire = true;
                    Destroy(gameObject);
                }
            }
        }        
    }
}
