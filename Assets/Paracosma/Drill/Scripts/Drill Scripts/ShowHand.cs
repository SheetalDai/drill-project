﻿using UnityEngine;
using VRTK;

public class ShowHand : MonoBehaviour
{
    public string hand;
    GameObject _Hand;
    public VRTK_ControllerEvents events;
    public VRTK_InteractGrab interactGrab;
    private void Start()
    {
        _Hand = GameObject.Find(hand);
        events.GripPressed += Events_GripPressed;
        events.GripReleased += Events_GripReleased;
    }

    private void Events_GripReleased(object sender, ControllerInteractionEventArgs e)
    {
        if (_Hand != null)
            _Hand.SetActive((interactGrab.GetGrabbedObject() == null) ? true : true);
    }

    private void Events_GripPressed(object sender, ControllerInteractionEventArgs e)
    {
        if (_Hand != null)
            _Hand.SetActive((interactGrab.GetGrabbedObject() != null) ? false : true);
    }

    private void Update()
    {
        if (_Hand == null)
        {
            _Hand = GameObject.Find(hand);
        }
        if (AutomaticSnap.haptic)
        {
            AutomaticSnap.haptic = false;
            var left = GameObject.Find("LeftControllerScriptAlias").GetComponent<VRTK_ControllerEvents>();
            var right = GameObject.Find("RightControllerScriptAlias").GetComponent<VRTK_ControllerEvents>();
            if (interactGrab.GetGrabbedObject().CompareTag("Drill"))
            {
                VRTK_ControllerHaptics.TriggerHapticPulse(VRTK_ControllerReference.GetControllerReference(right.gameObject), 100000 * 0.25f, 0.1f, 0.01f);
                VRTK_ControllerHaptics.TriggerHapticPulse(VRTK_ControllerReference.GetControllerReference(left.gameObject), 100000 * 0.25f, 0.1f, 0.01f);
            }
            else
            {
                VRTK_ControllerHaptics.TriggerHapticPulse(VRTK_ControllerReference.GetControllerReference(left.gameObject), 100000 * 0.25f, 0.1f, 0.01f);
                VRTK_ControllerHaptics.TriggerHapticPulse(VRTK_ControllerReference.GetControllerReference(right.gameObject), 100000 * 0.25f, 0.1f, 0.01f);
            }
        }
    }

}
