﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Heizenrader
{
    // This script is only used for the editor
    // Is is for attaching to control points, so that when
    // they are selected you can still see the original bezier tube
    public class BezierTubePoint : MonoBehaviour
    {
        public BezierTube bezierTube = null;
        public void Awake()
        {
            this.enabled = false;
        }

        public void Reset()
        {
            // convenience method for finding the bezier tube we belong to
            bezierTube = GetComponentInParent<BezierTube>();
        }
    }
}