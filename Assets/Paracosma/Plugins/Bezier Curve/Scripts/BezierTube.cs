﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Heizenrader
{

    [RequireComponent(typeof(MeshRenderer))]
    [RequireComponent(typeof(MeshFilter))]

    public class BezierTube : MonoBehaviour
    {
        [Header("Structure")]

        // Stacks are |
        // ---------------
        // |      |      |
        // ---------------

        [Tooltip("The number of vertical segments between each control point. Controls the quality of the mesh.")]
        public int stacks = 12;

        [Tooltip("The number of vertices in each stack. Controls the quality of the mesh.")]
        public int slices = 12;

        [Tooltip("The outer radius along the tube. Controls the size of the tubing.")]
        public float maxRadius = 0.05f;

        [Tooltip("This curve is used to control the radius along the tube. The max radius is multiplied by the curve to get the final radius.")]
        public AnimationCurve radiusCurve = AnimationCurve.Linear(0.0f, 1.0f, 1.0f, 1.0f);

        [Header("Mesh")]

        [Tooltip("Used to offset texture coordinates.")]
        public Vector2 uvOffset = Vector3.zero;

        [Tooltip("UV coordinates are over the entire tube. The X scale is along the length of the tube and the Y scale is around its radius. If you want the texture to repeat each stack, set the X scale to the number of stacks.")]
        public Vector2 uvScale = new Vector3(1.0f, 1.0f);

        [Tooltip("Chooses whether the mesh includes caps at the end of the tube. Without caps the mesh is left open.")]
        public bool includeEndCaps = false;

        [Tooltip("When this is enabled, the \"up\" direction of the model is used and does not change. When it is disabled the up direction flows with the curve of the model. Disabling it prevents creases and twisting. ")]
        public bool lockOrientation = false;

        // If the tube won't move around at runtime you can disable this to improve performance
        // when it is enabled the mesh vertices are moved everyframe
        // otherwise you can manually update it with TransformVertices
        public bool updateInRealtime = true;

        [Header("Editor Display")]

        [Tooltip("Shows the control handles for moving points.")]
        public bool drawHandles = true;
        [Tooltip("Shows the control points and lines in the editor.")]
        public bool drawDebugLines = true;
        [Tooltip("Shows the location of each vertex and its normal in the editor.")]
        public bool drawDebugVertices = false;

        [Header("Path Points")]
        public Transform[] endPoints;
        public Transform[] controlPoints;



        // cache of control point positions in local space
        int endPointCount = 0;
        Vector3[] cp = null;
        Vector3[] ep = null;
        Mesh tubeMesh = null;
        Vector3[] curveOrigins = null;
        Vector3[] curveTangents = null;

        public void Start()
        {
            var currentMesh = GetComponent<MeshFilter>().mesh;

            if (updateInRealtime)
            {
                UpdateMesh();
            }
            else
            {
                tubeMesh = currentMesh;

                if (tubeMesh == null)
                {
                    UpdateMesh();
                }
            }
        }
        private void Update()
        {
            UpdateMesh();
            UpdateEndPoints();
            TransformVertices();
        }
        // Update is called once per frame
        void LateUpdate()
        {
            if (updateInRealtime)
            {
                UpdateEndPoints();
                TransformVertices();
            }
        }

        // this is called to update the mesh as control points or end points are moved
        public void TransformVertices()
        {
            if (endPointCount < 1 || tubeMesh == null) return;

            var vertices = tubeMesh.vertices;
            var normals = tubeMesh.normals;
            var tangents = tubeMesh.tangents;
            var uvs = tubeMesh.uv;

            BezierTubeMath.buildCurve(ep, cp, endPointCount, stacks, curveOrigins, curveTangents);

            BezierTubeMath.buildTubeVertices(endPointCount, 
                                             stacks, 
                                             slices, 
                                             maxRadius, 
                                             radiusCurve, 
                                             uvScale, 
                                             uvOffset, 
                                             includeEndCaps, 
                                             lockOrientation, 
                                             vertices, 
                                             normals, 
                                             tangents, 
                                             uvs, 
                                             curveOrigins, 
                                             curveTangents);

            tubeMesh.vertices = vertices;
            tubeMesh.tangents = tangents;
            tubeMesh.normals = normals;
            tubeMesh.uv = uvs;
        }

        public void UpdateEndPoints()
        {
            if (endPoints.Length != endPointCount)
            {
                // reallocate only if the size changed
                endPointCount = endPoints.Length;
                ep = new Vector3[endPointCount];
                cp = new Vector3[endPointCount];
            }

            for (int i = 0; i < endPointCount; ++i)
            {
                if (endPoints[i] == null || controlPoints[i] == null) break;
                ep[i] = transform.InverseTransformPoint(endPoints[i].position);
                cp[i] = transform.InverseTransformPoint(controlPoints[i].position);
            }
        }

        // call this to update the mesh structure
        // after changing the number of stacks or slices
        public void UpdateMesh()
        {
            UpdateEndPoints();

            var meshFilter = GetComponent<MeshFilter>();
            tubeMesh = GenerateMesh(true, out this.curveOrigins, out this.curveTangents);
            meshFilter.mesh = tubeMesh;
        }

        public Mesh GenerateMesh(bool dynamic, out Vector3[] curveOrigins, out Vector3[] curveTangents)
        {
            curveOrigins = null;
            curveTangents = null;

            if (endPointCount < 1)
            {
                return null;
            }

            // Thes are not "hard limits" but extremely impractical
            // you can change them if needed
            const int maxStacks = 10000;
            const int maxSlices = 10000;

            if (stacks > maxStacks)
            {
                Debug.LogErrorFormat("Stacks or slices is too large, max: {0}", maxStacks);
                return null;
            }

            if (slices > maxSlices)
            {
                Debug.LogErrorFormat("Slices is too large, max: {0}", maxSlices);
                return null;
            }

            if (stacks < 2 || slices < 2)
            {
                return null;
            }

            curveOrigins = new Vector3[endPointCount * stacks];
            curveTangents = new Vector3[endPointCount * stacks];

            BezierTubeMath.buildCurve(ep, cp, endPointCount, stacks, curveOrigins, curveTangents);

            // build vertices, normals, uvs
            var vertCount = BezierTubeMath.getVertexCount(endPointCount, stacks, slices, includeEndCaps);
            var vertices = new Vector3[vertCount];
            var normals = new Vector3[vertCount];
            var tangents = new Vector4[vertCount];
            var uvs = new Vector2[vertCount];

            BezierTubeMath.buildTubeVertices(endPointCount, 
                                             stacks, 
                                             slices, 
                                             maxRadius, 
                                             radiusCurve, 
                                             uvScale, 
                                             uvOffset, 
                                             includeEndCaps, 
                                             lockOrientation,
                                             vertices, 
                                             normals, 
                                             tangents, 
                                             uvs, 
                                             curveOrigins, 
                                             curveTangents);

            // build triangle indicies
            var triangles = new int[BezierTubeMath.getTriangleIndexCount(endPointCount, stacks, slices, includeEndCaps)];
            BezierTubeMath.buildTubeTriangles(endPointCount, stacks, slices, includeEndCaps, triangles);

            // create a new mesh from the mesh data
            var newMesh = new Mesh();

            if (dynamic)
                newMesh.MarkDynamic();

            newMesh.vertices = vertices;
            newMesh.normals = normals;
            newMesh.tangents = tangents;
            newMesh.uv = uvs;
            newMesh.triangles = triangles;
            return newMesh;
        }
    }

    // All of the math needed for generating bezier tubes
    // This is separate from the Unity component so that it can be used by other code.
    // And so you can create your own component if you are not satisifed with the
    // way the default one is organized. (For example, perhaps you want to use an array of points rather than transforms)
    public static class BezierTubeMath
    {
        // returns the number of indicies which are in the triangle buffer
        // useful for allocating an array of proper size
        static public int getTriangleIndexCount(int endPointCount, int stacks, int slices, bool includeCaps)
        {
            // two triangles per slice make a quad, 3 vertices per triangle
            int count = endPointCount * stacks * slices * 2 * 3;

            if (includeCaps)
            {
                // each slice is one triangle and there are 2 caps
                count += slices * 3 * 2;
            }

            return count;  
        }

        // returns the number of vertices 
        // useful for allocating an array of proper size
        static public int getVertexCount(int endPointCount, int stacks, int slices, bool includeCaps)
        {
            int count = getBodyVertexCount(endPointCount, stacks, slices);

            if (includeCaps)
            {
                count += (slices + 1) * 2;
            }

            return count;
        }

        static int getBodyVertexCount(int endPointCount, int stacks, int slices)
        {
            // each curve shares a stack, except the last one (which is the + 1)
            return ((endPointCount - 1) * (stacks - 1) + 1) * (slices + 1);
        }

        // build triangle indices, only needs to be done once per mesh since 
        // indicies are not dependant on vertex position
        static public void buildTubeTriangles(int endPointCount, int stacks, int slices, bool includeCaps, int[] triangles)
        {
            int iIndex = 0;
            int vIndex = 0;

            for (int i = 0; i < endPointCount - 1; ++i)
            {
                for (int j = 0; j < stacks - 1; ++j)
                {
                    // stack start index
                   // int vIndexForStack = (j * (slices + 1)) + (i * (slices + 1) * (stacks - 1)); 

                    for (int k = 0; k < slices; ++k)
                    {
                        // triangle 1
                        triangles[iIndex] = vIndex + k;
                        triangles[iIndex + 2] = vIndex + (slices + 1) + k;
                        triangles[iIndex + 1] = vIndex + k + 1;
                        iIndex += 3;
                        
                        // triangle 2
                        triangles[iIndex] = vIndex + k + 1;
                        triangles[iIndex + 2] = vIndex + (slices + 1) + k;
                        triangles[iIndex + 1] = vIndex + (slices + 1) + k + 1;
                        iIndex += 3;
                    }

                    vIndex += slices + 1;
                }
            }

            vIndex += slices + 1;

            if (includeCaps)
            {
                // end cap
                int originIndex = vIndex;
                for (int i = 0; i < slices; ++i)
                {
                    triangles[iIndex] = vIndex;
                    triangles[iIndex + 1] = vIndex + 1;
                    triangles[iIndex + 2] = originIndex;
                    iIndex += 3;
                    ++vIndex;
                }

                ++vIndex;

                originIndex = vIndex;
                for (int i = 0; i < slices; ++i)
                {
                    triangles[iIndex] = vIndex + 1;
                    triangles[iIndex + 1] = vIndex;
                    triangles[iIndex + 2] = originIndex;
                    iIndex += 3;
                    ++vIndex;
                }
            }
        }

        // build a tube/cylinder that fits on the bezier curve.
        static public void buildTubeVertices(int endPointCount,
                                             int stacks,            // the number of stackes (horizontal segements) in the tube
                                             int slices,            // the number of slices (vertical segments) in the tube. Determines how round it is
                                             float maxRadius,          // The maximum radius of the tube/cylinder to generate
                                             AnimationCurve radiusCurve, // The radius function curve
                                             Vector2 uvScale,       // For repeating the texture on any axis (1.0, 1.0) is normal.
                                             Vector2 uvOffset,      // For offseting the texture (0.0, 0.0) is normal.
                                             bool includeCaps,
                                             bool lockOrientation,
                                             Vector3[] vertices,    // array of vertices to fill
                                             Vector3[] normals,     // array of normals to fill
                                             Vector4[] tangents,    // array of tangents to fill
                                             Vector2[] uvs,         // array of UVs to fill
                                             Vector3[] nodes,     // points of the beizer curve (unmodified)
                                             Vector3[] nodeTangents)    // tangents of the bezier curve (unmodified)
        {
            Vector3 spaceUp = Vector3.up;

            int vIndex = 0;
            for (int i = 0; i < endPointCount - 1; ++i)
            {
                // each curve shares a stack, except the last one 
                int segments = (i == endPointCount - 2) ? stacks : stacks - 1;
                for (int j = 0; j < segments; ++j)
                {
                    int pIndex = j + i * (stacks - 1);

                    // construct matrix from basis vectors
                    var xPrime = Vector3.Cross(spaceUp, nodeTangents[pIndex]).normalized;
                    var yPrime = Vector3.Cross(nodeTangents[pIndex], xPrime).normalized; 
                    var matrix = new Matrix4x4(xPrime, yPrime, nodeTangents[pIndex], nodes[pIndex]);

                    if (!lockOrientation)
                    {
                        // instead of using a universal up, the up flows with the model
                        spaceUp = new Vector3(yPrime.x, yPrime.y * 1.1f, yPrime.z);
                    }

                    int c = (endPointCount - 1) * (stacks - 1) + 1;
                    float lengthInterp = (j + i * (stacks - 1)) / (float)c;

                    float radius = maxRadius;
                    if (radiusCurve != null) radius *= radiusCurve.Evaluate(lengthInterp);

                    // there is an extra slice, with the same position as the first, at the seam.
                    // so that both have separate UV coordinates
                    for (int k = 0; k < slices + 1; ++k)
                    {
                        float angleTime = k / (float)slices;
                        float theta = Mathf.PI * 2.0f * angleTime;

                        var local = new Vector3(Mathf.Cos(theta) * radius, Mathf.Sin(theta) * radius, 0.0f);
                        vertices[vIndex] = matrix.MultiplyPoint3x4(local);

                        if (normals != null)
                        {
                            normals[vIndex] = matrix.MultiplyVector(local).normalized;
                        }

                        if (tangents != null)
                        {
                            // normals are constructed along basis with length one
                            Vector4 t = nodeTangents[pIndex];
                            t.w = -1.0f;
                            tangents[vIndex] = t;
                        }

                        if (uvs != null)
                        {
                            // UVs are easy, x: 0.0 for left of tube, x: 1.0 for right of tube similar for vertical
                            uvs[vIndex] = new Vector2(lengthInterp * uvScale.x + uvOffset.x, angleTime * uvScale.y + uvOffset.y);
                        }
                        ++vIndex;
                    }
                }
            }

            if (includeCaps)
            {
                // end cap
                int startIndex = vIndex - slices - 1;
                for (int k = 0; k < slices + 1; ++k)
                {
                    // copy last stack's vertices with different normals
                    vertices[vIndex] = vertices[startIndex + k];
                    normals[vIndex] = tangents[k];
                    tangents[vIndex] = new Vector4(nodeTangents[endPointCount - 1].z, 0.0f, nodeTangents[endPointCount - 1].x, 1.0f).normalized;

                    uvs[vIndex] = uvs[startIndex + k];
                    ++vIndex;
                }

                // front cap
                for (int k = 0; k < slices + 1; ++k)
                {
                    // copy first stack vertices with different normals
                    vertices[vIndex] = vertices[k];
                    normals[vIndex] = -tangents[k];
                    tangents[vIndex] = new Vector4(nodeTangents[0].z, 0.0f, -nodeTangents[0].x, 1.0f).normalized;

                    uvs[vIndex] = uvs[k];
                    ++vIndex;
                }
            }
        }

        // calculate the points and tangents of a bezier curve
        static public void buildCurve(Vector3[] endPoints,
                                      Vector3[] controlPoints,
                                      int endPointCount,
                                      int stacks, // per section
                                      Vector3[] nodes,
                                      Vector3[] nodeTangents)
        {
            var p1 = endPoints[0];
            var p2 = controlPoints[0];

            for (int i = 1; i < endPointCount; ++i)
            {
                var p3 = controlPoints[i];
                var p4 = endPoints[i];

                // every curve uses the starting stack of the next curve as its end, until the last
                // on the last one we need one more stack
                int segments = (i == endPointCount - 1) ? stacks : stacks - 1;
                for (int j = 0; j < segments; ++j)
                {
                    float t = j / (float)(stacks - 1);

                    int pIndex = j + (i - 1) * (stacks - 1);

                    // standard bezier curve coeffecients
                    float k1 = (1.0f - t) * (1.0f - t) * (1.0f - t);
                    float k2 = 3.0f * t * (1.0f - t) * (1.0f - t);
                    float k3 = 3.0f * t * t * (1.0f - t);
                    float k4 = t * t * t;
                    nodes[pIndex] = (k1 * p1) + (k2 * p2) + (k3 * p3) + (k4 * p4);

                    // gradient of the bezier curve gives tangents to the curve
                    float tk1 = -3.0f * (1.0f - t) * (1.0f - t);
                    float tk2 = 3.0f * (1.0f - t) * (1.0f - t) - 6.0f * t * (1.0f - t);
                    float tk3 = 6.0f * t - 9.0f * t * t;
                    float tk4 = 3.0f * t * t;
                    nodeTangents[pIndex] = ((tk1 * p1) + (tk2 * p2) + (tk3 * p3) + (tk4 * p4)).normalized;
                }

                p1 = p4;
                // control point comes off in the opposite direction 
                p2 = p4 - (p3 - p4);
            }
        }
    }
}