﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Heizenrader;

[CustomEditor(typeof(BezierTubePoint))]
public class BezierTubePointEditor : Editor
{
    void OnSceneGUI()
    {
        BezierTubePoint t = target as BezierTubePoint;

        if (t != null)
        {
            if (t.bezierTube != null)
            {
                BezierTubeEditor.DrawTubeEditor(t.bezierTube);
            }
            else
            {
                var tube = t.GetComponentInParent<BezierTube>();
                BezierTubeEditor.DrawTubeEditor(tube);
            }
        }
    }
}

[CustomEditor(typeof(BezierTube))]
public class BezierTubeEditor : Editor
{
    void OnSceneGUI()
    {
        BezierTube t = target as BezierTube;
        BezierTubeEditor.DrawTubeEditor(t);

        if (t.drawHandles)
        {
            Handles.color = Color.yellow;

            for (int i = 0; i < t.endPoints.Length; ++i)
            {
                if (t.endPoints[i] == null) break;
                var point = t.endPoints[i];
                var size = HandleUtility.GetHandleSize(point.position);

                EditorGUI.BeginChangeCheck();
                Vector3 pos = Handles.FreeMoveHandle(point.position, Quaternion.identity, size * 0.1f, new Vector3(.5f, .5f, .5f), Handles.CubeHandleCap);
                if (EditorGUI.EndChangeCheck())
                {
                    Undo.RecordObject(point, "Bezier point");
                    point.position = pos;
                }
            }

            for (int i = 0; i < t.controlPoints.Length; ++i)
            {
                if (t.controlPoints[i] == null) break;

                var point = t.controlPoints[i];
                var size = HandleUtility.GetHandleSize(point.position);

                EditorGUI.BeginChangeCheck();
                Vector3 pos = Handles.FreeMoveHandle(point.position, Quaternion.identity, size * 0.1f, new Vector3(.5f, .5f, .5f), Handles.CubeHandleCap);
                if (EditorGUI.EndChangeCheck())
                {
                    Undo.RecordObject(point, "Bezier control point");
                    point.position = pos;
                }
            }
        }
    }

    static public void DrawTubeEditor(BezierTube t)
    {
        if (t.stacks < 2) return;
        if (t.slices < 2) return;

        if (t.endPoints.Length != t.controlPoints.Length) return;

        var ep = new Vector3[t.endPoints.Length];
        var cp = new Vector3[t.endPoints.Length];

        int endPointCount = 0;

        for (int i = 0; i < t.endPoints.Length; ++i)
        {
            if (t.endPoints[i] == null || t.controlPoints[i] == null) break;
            ep[i] = t.endPoints[i].position;
            cp[i] = t.controlPoints[i].position;
            ++endPointCount;
        }

        if (endPointCount < 1) return;

        var origins = new Vector3[t.stacks * endPointCount];
        var tangents = new Vector3[t.stacks * endPointCount];

        BezierTubeMath.buildCurve(ep, cp, endPointCount, t.stacks, origins, tangents);

        if (t.drawDebugLines)
        {
            var p1 = ep[0];
            var p2 = cp[0];

            for (int i = 1; i < endPointCount; ++i)
            {
                var p3 = cp[i];
                var p4 = ep[i];
                Handles.DrawLine(p1, p2);
                Handles.DrawLine(p3, p4);
                p1 = p4;
                p2 = p4 - (p3 - p4);
            }


            Vector3 spaceUp = Vector3.up;

            for (int i = 0; i < endPointCount; ++i)
            {
                for (int j = 0; j < t.stacks; ++j)
                {
                    int pIndex = j + i * (t.stacks - 1);

                    Vector3 tangent = tangents[pIndex];
                    Vector3 xPrime = Vector3.Cross(spaceUp, tangent).normalized;
                    Vector3 yPrime = Vector3.Cross(tangent, xPrime).normalized; 

                    if (!t.lockOrientation)
                    {
                        spaceUp = new Vector3(yPrime.x, yPrime.y * 1.1f, yPrime.z);
                    }

                    Handles.color = Color.blue;
                    Handles.DrawLine(origins[pIndex], origins[pIndex] + tangent * t.maxRadius);
                    Handles.color = Color.red;
                    Handles.DrawLine(origins[pIndex], origins[pIndex] + xPrime * t.maxRadius);
                    Handles.color = Color.green;
                    Handles.DrawLine(origins[pIndex], origins[pIndex] + yPrime * t.maxRadius);
                    Handles.color = Color.white;
                }
            }
        }


        if (t.drawDebugVertices)
        {
            // build vertices, normals, uvs
            var vertCount = BezierTubeMath.getVertexCount(endPointCount, t.stacks, t.slices, false);
            var vertices = new Vector3[vertCount];
            var normals = new Vector3[vertCount];

            float size = HandleUtility.GetHandleSize(t.transform.position);

            BezierTubeMath.buildTubeVertices(endPointCount, t.stacks, t.slices, t.maxRadius, t.radiusCurve, t.uvScale, t.uvOffset, false, t.lockOrientation, vertices, normals, null, null, origins, tangents);

            for (int i = 0; i < vertCount; ++i)
            {
                Handles.DrawLine(vertices[i], vertices[i] + normals[i] * size * 0.05f);
            }
        }
    }


    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        var tube = target as BezierTube;

        if (GUILayout.Button("Bake"))
        {
            tube.UpdateEndPoints();
            var meshFilter = tube.GetComponent<MeshFilter>();

            Vector3[] curveOrigins = null;
            Vector3[] curveTangents = null;
            meshFilter.sharedMesh = tube.GenerateMesh(false, out curveOrigins, out curveTangents);
        }
    }
}
