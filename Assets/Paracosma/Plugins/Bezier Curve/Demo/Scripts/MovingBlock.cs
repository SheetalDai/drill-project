﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingBlock : MonoBehaviour
{
    public float amplitude = 1.0f;
    public float initialTime;
    public Vector3 direction = Vector3.forward;
    public float speed = 1.0f;

    Vector3 origin;

    public void Start()
    {
        origin = transform.localPosition;
    }

    // Update is called once per frame

	// Update is called once per frame
	void Update ()
    {
        var t = Time.time * speed;
        transform.localPosition = origin + direction * Mathf.Sin(t) * amplitude;
	}
}
