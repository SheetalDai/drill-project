﻿using UnityEngine;
using VRTK;

public class AutomaticSnap : MonoBehaviour
{
    VRTK_SnapDropZone sZone;
    public static bool haptic;
    private void Start()
    {
        sZone = GetComponent<VRTK_SnapDropZone>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if(sZone.ValidSnappableObjectIsHovering())
        {
            sZone.ForceSnap(other.gameObject);
            haptic = true;
            DrillSoundManager.Instance.HosePluggedClip();
        }        
    }
}
