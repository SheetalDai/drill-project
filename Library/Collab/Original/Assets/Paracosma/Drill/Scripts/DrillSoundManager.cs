﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using VRTK;

public class DrillSoundManager : MonoBehaviour
{

    public AudioSource bgSource, bgSource2, drillSource, menuSource;
    public AudioClip[] metalClip;
    public AudioClip drillClip1, drillClip2, drillClip3,drillMetallic; //drill sound clip    
    public AudioClip popIn, popOut, sucess, click, pluged; //menu sound clip

    private static DrillSoundManager _instance;
    public static DrillSoundManager Instance { get { return _instance; } }
    private void Start()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
        InvokeRepeating("MetalClip", 1, 10);
    }
    //bgsourece 2 metal bg sound
    public void MetalClip()
    {
        int r = Random.Range(0, 3);
        bgSource2.clip = metalClip[r];
        bgSource2.Play();
    }

    //drill sound 
    public void PlayDrillClip1()
    {
        drillSource.clip = drillClip1;
        drillSource.Play();
        Debug.Log(drillSource.time);
        //Invoke("PlayDrillClip2", drillClip1.length);        
    }

    public void PlayDrillClip2()
    {
        drillSource.clip = drillClip2;
        drillSource.loop = true;
        drillSource.Play();
    }

    public void PlayDrillClip3()
    {
        drillSource.Stop();
        drillSource.clip = drillClip3;
        drillSource.loop = false;
        drillSource.Play();
    }
    public void PlayDrillMetallic()
    {
        drillSource.Stop();
        drillSource.clip = drillMetallic;
        drillSource.loop = true;
        drillSource.Play();
    }
    public void StopDrillClip()
    {
        drillSource.Stop();
    }

    //menu sounds
    public void PopInClip()
    {
        menuSource.PlayOneShot(popIn);
    }
    public void PopOutClip()
    {
        menuSource.PlayOneShot(popOut);
    }
    public void SuccessClip()
    {
        menuSource.PlayOneShot(sucess);
    }
    public void BtnClickClip()
    {
        menuSource.PlayOneShot(click);
    }
    public void HosePluggedClip()
    {
        menuSource.PlayOneShot(pluged);
    }
}
