﻿using UnityEngine;

public class LineConnecter : MonoBehaviour
{
    LineRenderer lr;
    public Transform[] pos;
    public GameObject[] bones;
    public bool test;

    void Start()
    {
        lr = GetComponent<LineRenderer>();
        for (int j = 1; j < pos.Length; j++)
        {
            pos[j].GetComponent<CharacterJoint>().connectedBody = pos[j - 1].GetComponent<Rigidbody>();
        }
    }

        void Update()
        {
            lr.positionCount = pos.Length;
            for (int i = 0; i < pos.Length; i++)
            {
                lr.SetPosition(i, pos[i].position);
            }
        }
    }

