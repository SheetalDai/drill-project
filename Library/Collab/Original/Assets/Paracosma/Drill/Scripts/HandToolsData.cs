﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandToolsData : MonoBehaviour
{
    public static HandToolsData instance;
    public int TotalItemNumbers=10;
    public int value;
    public string[] handToolsName;
    public string[] handToolsDescription;
    // Start is called before the first frame update
    void Start()
    {
        instance = this;
        for (int i = 0; i < TotalItemNumbers; i++)
        {
            value = i;
            Data();
        }
        
    }

    
    public void Data()
    {
        switch (value)
        {
            case 0:
                handToolsName[0] = "Drill";
                handToolsDescription[0] = "In this simulation, trainees learn and demonstrate proper procedures for use of a drill. After learning the proper procedures for use of a drill, the trainee demonstrates their knowledge with corrective feedback from the virtual coach to guide the trainee toward mastery.";
                break;
            case 1:
                handToolsName[1] = "Circular Saw";
                handToolsDescription[1] = "In this simulation, trainees learn and demonstrate proper procedures for use of a circular saw. After learning the proper procedures for use of a circular saw, the trainee demonstrates their knowledge with corrective feedback from the virtual coach to guide the trainee toward mastery.";
                break;
            case 2:
                handToolsName[2] = "Angle Grinder";
                handToolsDescription[2] = "In this simulation, trainees learn and demonstrate proper procedures for use of an angle grinder. After learning the proper procedures for use of an angle grinder, the trainee demonstrates their knowledge with corrective feedback from the virtual coach to guide the trainee toward mastery.";
                break;
            case 3:
                handToolsName[3] = "Jigsaw";
                handToolsDescription[3] = "In this simulation, trainees learn and demonstrate proper procedures for use of a jigsaw. After learning the proper procedures for use of a jigsaw, the trainee demonstrates their knowledge with corrective feedback from the virtual coach to guide the trainee toward mastery.";
                break;
            case 4:
                handToolsName[4] = "Chainsaw";
                handToolsDescription[4] = "In this simulation, trainees learn and demonstrate proper procedures for use of a chainsaw. After learning the proper procedures for use of a chainsaw, the trainee demonstrates their knowledge with corrective feedback from the virtual coach to guide the trainee toward mastery.";
                break;
            case 5:
                handToolsName[5] = "Impact Wrench";
                handToolsDescription[5] = "In this simulation, trainees learn and demonstrate proper procedures for use of an impact wrench. After learning the proper procedures for use of an impact wrench, the trainee demonstrates their knowledge with corrective feedback from the virtual coach to guide the trainee toward mastery.";
                break;
            case 6:
                handToolsName[6] = "Hammer";
                handToolsDescription[6] = "In this simulation, trainees learn and demonstrate proper procedures for use of a hammer. After learning the proper procedures for use of a hammer, the trainee demonstrates their knowledge with corrective feedback from the virtual coach to guide the trainee toward mastery.";
                break;
            case 7:
                handToolsName[7] = "Screwdriver";
                handToolsDescription[7] = "In this simulation, trainees learn and demonstrate proper procedures for use of a screwdriver. After learning the proper procedures for use of a screwdriver, the trainee demonstrates their knowledge with corrective feedback from the virtual coach to guide the trainee toward mastery.";
                break;
            case 8:
                handToolsName[8] = "Utility Knife";
                handToolsDescription[8] = "In this simulation, trainees learn and demonstrate proper procedures for use of a utility knife. After learning the proper procedures for use of a utility knife, the trainee demonstrates their knowledge with corrective feedback from the virtual coach to guide the trainee toward mastery.";
                break;
            case 9:
                handToolsName[9] = "Chisel";
                handToolsDescription[9] = "In this simulation, trainees learn and demonstrate proper procedures for use of a chisel. After learning the proper procedures for use of a chisel, the trainee demonstrates their knowledge with corrective feedback from the virtual coach to guide the trainee toward mastery.";
                break;

        }
    }
}
